﻿using System;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public event Action<float> VerticalAxisСhanged;
    public event Action<float> HorizontalAxisСhanged;
    public event Action<float> MouseXAxisAxisСhanged;
    
    private float _verticalAxis;
    private float _horizontalAxis;
    private float _mouseXAxis;
    private float _mouseSensitivityHor = 4f;
    public float VerticalAxis
    {
        get => _verticalAxis;

        set
        {
            _verticalAxis = value;
            VerticalAxisСhanged?.Invoke(value);
        }
    }

    public float HorizontalAxis
    {
        get => _horizontalAxis;

        set
        {
            _horizontalAxis = value;
            HorizontalAxisСhanged?.Invoke(value);
        }
    }

    public float MouseXAxis
    {
        get => _mouseXAxis;

        set
        {
            _mouseXAxis = value;
            MouseXAxisAxisСhanged?.Invoke(value);
        }
    }

    void Update()
    {
        VerticalAxis = Input.GetAxis("Vertical");
        HorizontalAxis = Input.GetAxis("Horizontal");
        MouseXAxis = Input.GetAxis("Mouse X") * _mouseSensitivityHor;
    }
}