﻿using UnityEngine;

namespace Scripts.Scriptable_Objects
{
    [CreateAssetMenu(fileName = "Player_Start_Data", menuName = "Player_Start_Data", order = 52)]
    public class PlayerStartData: ScriptableObject
    {
        [SerializeField] private bool _isPlayerActive;
        [SerializeField] private int _hp;
        [SerializeField] private int currentLevel;
        [SerializeField] private float _speed;
        [SerializeField] private float _gravity;
        [SerializeField] private Vector3 _startPosition;
        
        public bool IsPlayerActive
        {
            get
            {
                return _isPlayerActive;
            }
        }
        
        public int Hp
        {
            get
            {
                return _hp;
            }
            set => _hp = value;
        }

        public int CurrentLevel
        {
            get
            {
                return currentLevel;
            }

            set { currentLevel = value; }
        }
        
        public float Speed
        {
            get
            {
                return _speed;
            }
        }
        
        public float Gravity
        {
            get
            {
                return _gravity;
            }
        }
        
        public Vector3 StartPosition
        {
            get
            {
                return _startPosition;
            }
        }
    }
}