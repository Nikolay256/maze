﻿using System;
using UnityEngine;

namespace Scripts.Scriptable_Objects
{
    [CreateAssetMenu(fileName = "Labyrinth_Data", menuName = "Labyrinth_Data", order = 51)]
    public class LabyrinthData: ScriptableObject
    {
        enum Directions {Up = 0b1000, Left = 0b0100, Right = 0b0010, Down = 0b0001}
        
        [Header("Лабиринт"), SerializeField] private int _numLines;
        [SerializeField] private int _numColumns;
        [SerializeField] private GameObject _wallPrefab;
        [SerializeField] private GameObject _florePrefab;
        
        [Header("Ловушки"), SerializeField] private int _numStakes;
        [SerializeField] private GameObject _stakesPrefab;
        
        [Header("Дверь"), SerializeField] private GameObject _doorPrefab;
        [SerializeField] private bool _doorAvtoSet;
        [SerializeField] private int _numDoorLine;
        [SerializeField] private int _numDoorColumn;
        [SerializeField] private Directions _doorDirection;
        
        public int NumLines => _numLines;
        public int NumColumns => _numColumns;
        public GameObject WallPrefab => _wallPrefab;
        public GameObject FlorePrefab => _florePrefab;

        public int NumStakes => _numStakes;
        public GameObject StakesPrefab => _stakesPrefab;

        public int NumDoorLine
        {
            get
            {
                if (_doorAvtoSet)
                    return _numLines - 1;
                return _numDoorLine;
            }
        }

        public int NumDoorColumn
        {
            get
            {
                if (_doorAvtoSet)
                    return _numColumns / 2;
                return _numDoorColumn;
            }
        }
    
        public byte DoorDirection
        {
            get
            {
                if (_doorAvtoSet)
                    return (byte)Directions.Down;
                return (byte)_doorDirection;
            }
        }
        
        public GameObject DoorPrefab => _doorPrefab;
    }
}