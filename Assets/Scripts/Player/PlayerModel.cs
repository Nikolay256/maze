﻿using System;
using Scripts.Auxiliary_Classes;

namespace Scripts.Player
{
    public class PlayerModel
    {
        public event Action<int> HpChanged;
        public event Action<float> MouseHorAxisChanged;
        public event Action<MyVector3> LastMoveChanged;
        private event Action<float, float> KeyBoardAxisChanged;

        public int CurrentLevel { get; set; }
        private bool _isPlayerActive;

        private bool _isGrounded = true;
        private float _reboundPower = 40f;
        
        private int _hp;
        public int Hp
        {
            get => _hp;
            set
            {
                if (_hp == value) return;
                _hp = value;
                HpChanged?.Invoke(value);
            }
        }
        
        private float _horAxis;
        public float HorAxis
        {
            get => _horAxis;
            set
            {
                _horAxis = value;
                KeyBoardAxisChanged?.Invoke(_horAxis, _verAxis);
            }
        }

        private float _verAxis;
        public float VerAxis
        {
            get => _verAxis;
            set
            {
                _verAxis = value;
                KeyBoardAxisChanged?.Invoke(_horAxis, _verAxis);
            }
        }

        private float _mouseHorAxis;
        public float MouseHorAxis
        {
            get => _mouseHorAxis;
            set
            {
                if (_mouseHorAxis == value) return;
                _mouseHorAxis = value;
                MouseHorAxisChanged?.Invoke(value);
            }
        }
        
        private MyVector3 _lastMove = new MyVector3(0f, 0f, 0f);
        public MyVector3 LastMove
        {
            get => _lastMove;
            set
            {
                _lastMove = value;
                LastMoveChanged?.Invoke(_lastMove);
            }
        }
        
        public float Speed { get; set; }
        public float Gravity { get; set; }
        public MyVector3 StartPosition { get; set; }

        public PlayerModel()
        {
            KeyBoardAxisChanged += MoveCalc;
        }

        public void SetHorAxis(float f)
        {
            HorAxis = f;
        }
        
        public void SetVerAxis(float f)
        {
            VerAxis = f;
        }
        
        public void SetMouseHorAxis(float f)
        {
            MouseHorAxis = f;
        }
        
        public void GetDamage(int a)
        {
            int tempHp = 0;
            if (a == 10)
            {
                tempHp = Hp - 3;
            }

            if (tempHp <= 0)
            {
                Hp = 0;
                _isPlayerActive = false;
                return;
            }

            Hp = tempHp;
            Rebound();
        }

        private void MoveCalc(float hAxis, float vAxis)
        {
            MyVector3 tempMove = new MyVector3(0f, 0f, 0f);
            if (_isGrounded)
            {
                tempMove.X = hAxis * Speed;
                tempMove.Z = vAxis * Speed;
            }
            tempMove.LineClamp(Speed);
            tempMove.Y -= Gravity;
            LastMove = tempMove;
        }

        private void Rebound()
        {
            LastMove = new MyVector3(-LastMove.X, 1f, -LastMove.Z) * _reboundPower;
        }
    }
}