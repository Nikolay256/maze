﻿namespace Scripts.Player
{
    public class PlayerController
    {
        public PlayerModel PlayerModel{get; set;}
        public PlayerView PlayerView{get; set;}
        public InputController InputController{get; set;}
        
        
        public PlayerController(){}
        public PlayerController(PlayerModel playerModel, PlayerView playerView, InputController inputController)
        {
            PlayerView = playerView;
            PlayerModel = playerModel;
            InputController = inputController;
        }
        
        public void Binding()
        {
            PlayerModel.LastMoveChanged += PlayerView.Move;
            PlayerModel.MouseHorAxisChanged += PlayerView.UpdateMouseAxis;
            PlayerView.StepOnStakes += PlayerModel.GetDamage;

            InputController.VerticalAxisСhanged += PlayerModel.SetVerAxis;
            InputController.HorizontalAxisСhanged += PlayerModel.SetHorAxis;
            InputController.MouseXAxisAxisСhanged += PlayerModel.SetMouseHorAxis;
        }
        
        public void Unbinding()
        {
            PlayerModel.LastMoveChanged -= PlayerView.Move;
            PlayerModel.MouseHorAxisChanged -= PlayerView.UpdateMouseAxis;
            PlayerView.StepOnStakes -= PlayerModel.GetDamage;
            
            InputController.VerticalAxisСhanged -= PlayerModel.SetVerAxis;
            InputController.HorizontalAxisСhanged -= PlayerModel.SetHorAxis;
            InputController.MouseXAxisAxisСhanged -= PlayerModel.SetMouseHorAxis;
        }
    }
}