﻿using System;
using UnityEngine;
using Scripts.Auxiliary_Classes;

namespace Scripts.Player
{
    public class PlayerView : MonoBehaviour
    {
        public Vector3 StartPosition;
        
        private CharacterController _characterController;
        public event Action<int> StepOnStakes;
        
        
        public void UpdateMouseAxis(float mouseHorAxis) => Rotate(mouseHorAxis);

        public void Start()
        {
            StartPosition = new Vector3(0.2f, 0.5f, 0.2f);///TODO задавать стартовую позицию не в ручную
            gameObject.transform.position = StartPosition;
            _characterController = GetComponent<CharacterController>();
        }

        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            if (hit.gameObject.layer == 10)
            {
                StepOnStakes?.Invoke(10);
            }
        }
        
        public void Move(MyVector3 newMove)
        {
            Vector3 move = new Vector3(newMove.X, newMove.Y, newMove.Z) * Time.deltaTime;
            move = gameObject.transform.TransformDirection(move);
            _characterController.Move(move);
        }
        
        private void Rotate(float mouseHorAxis)
        {
            gameObject.transform.Rotate(0, mouseHorAxis, 0);
        }
    }
}