﻿using Scripts.Player;

namespace Scripts.Interface
{
    public class InterfaceController
    {
        private PlayerModel _playerModel;
        private InterfaceView _interfaceView;
        public InterfaceController(PlayerModel playerModel, InterfaceView interfaceView)
        {
            interfaceView._textHP.text = "HP: " + playerModel.Hp;
            interfaceView._textLVL.text = "LeveL: " + playerModel.CurrentLevel;
            _playerModel = playerModel;
            _interfaceView = interfaceView;
        }
        
        public void Binding()
        {
            _playerModel.HpChanged += _interfaceView.RewriteHP;
        }
        
        public void Unbinding()
        {
            _playerModel.HpChanged -= _interfaceView.RewriteHP;
        }
    }
}