﻿using UnityEngine;
using UnityEngine.UI;

namespace Scripts.Interface
{
    public class InterfaceView : MonoBehaviour
    {
        public Text _textHP;
        public Text _textLVL;
        

        public void RewriteHP(int newHP)
        {
            _textHP.text = "HP: " + newHP;
        }
    }
}