﻿using UnityEngine;
 
public class MyCamera : MonoBehaviour
{
    public Transform target;
    private Vector3 _position;
 
    void Start ()
    {
        _position = target.InverseTransformPoint(transform.position);
    }
	
    void Update ()
    {
        var oldRotation = target.rotation;
        target.rotation = Quaternion.Euler(0, oldRotation.eulerAngles.y, 0);
        var currentPosition = target.TransformPoint(_position);
        target.rotation = oldRotation;

        transform.position = currentPosition;
        var currentRotation = Quaternion.LookRotation(target.position - transform.position);
        transform.rotation = currentRotation;

        if (Physics.Raycast(target.position, transform.position - target.position, out RaycastHit hit, Vector3.Distance(transform.position, target.position)))
        {
            transform.position = hit.point;
            transform.LookAt(target);
        }
    }
}