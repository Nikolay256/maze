﻿using Scripts.Auxiliary_Classes;
using Scripts.Player;
using Scripts.Scriptable_Objects;
using UnityEngine;

namespace ScenesManager
{
    public class StartManger : MonoBehaviour
    {
        [SerializeField] private PlayerStartData _playerStartData;
        [SerializeField] private Canvas _deathCanvas;
        private PlayerModel _playerModel;
        private PlayerController _playerController;
        private GameSceneManager _sceneManager;
        private InputController _inputController;
        private void Start()
        {
            _sceneManager = new GameSceneManager {_corutineLaunch = GetComponent<СoroutineLaunch>()};
            _sceneManager.CompletedLoad += OnSceneManagerCompleted;
            
            _inputController = gameObject.GetComponent<InputController>();

            StartNewGame();
        }
        
        private void OnSceneManagerCompleted(SceneController controller)
        {
            controller.Init(_playerModel, _inputController, _playerController, LoadNextLvl);
        }

        private void LoadNextLvl()
        {
            _sceneManager.LoadScene("Game", "Game");
        }

        private void FillPlayer()
        {
            _playerModel.StartPosition = new MyVector3(_playerStartData.StartPosition.x, 
                _playerStartData.StartPosition.y,
                _playerStartData.StartPosition.z);
            _playerModel.CurrentLevel = _playerStartData.CurrentLevel;
            //_playerModel.IsPlayerActive = _playerStartData.IsPlayerActive;
            _playerModel.Hp = _playerStartData.Hp;
            _playerModel.Speed = _playerStartData.Speed;
            _playerModel.Gravity = _playerStartData.Gravity;
            _playerModel.HpChanged += DeathOpen;
        }

        public void StartNewGame()
        {
            _playerModel = new PlayerModel();
            _playerController = new PlayerController {InputController = _inputController, PlayerModel = _playerModel};
            FillPlayer();
            _deathCanvas.gameObject.SetActive(false);
            LoadNextLvl();
        }

        public void DeathOpen(int HP)
        {
            if(HP <= 0)
            {
                
                _deathCanvas.gameObject.SetActive(true);
                _playerController.Unbinding();
            }
        }
    }
}