﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ScenesManager
{
    public class GameSceneManager
    {
        public event Action<SceneController> CompletedLoad;
        public СoroutineLaunch _corutineLaunch;
        
        private event Action CompletedUnload;
        
        private string _currentLoadScene;
        private string _currentUnloadScene;
        private string _currentScenScript;

        public void LoadScene(string sceneName, string objScenScript)
        {
            _currentLoadScene = sceneName;
            _currentScenScript = objScenScript;
            if (SceneManager.sceneCount == 2)
            {
                CompletedUnload += LoadSceneCor;
                UnloadScene(SceneManager.GetSceneAt(1).name);
            }
            else
            {
                LoadSceneCor();
            }
        }

        private void LoadSceneCor()
        {
            CompletedUnload = null;
            IEnumerator cor = LoadAsyncScene();
            _corutineLaunch.StartCoroutine(cor);
        }
        
        private void UnloadScene(string sceneName)
        {
            _currentUnloadScene = sceneName;
            IEnumerator cor = UnloadAsyncScene();
            _corutineLaunch.StartCoroutine(cor);
        }

        public IEnumerator LoadAsyncScene()
        {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(_currentLoadScene, LoadSceneMode.Additive);

            while (!asyncLoad.isDone)
            {
                yield return null;
            }

            var scenesObj = SceneManager.GetSceneByName(_currentLoadScene).GetRootGameObjects();
            foreach (var obj in scenesObj)
            {
                if (obj.name == _currentScenScript)
                {
                    CompletedLoad?.Invoke(obj.GetComponent<SceneController>());
                    break;
                }
            }
        }

        private IEnumerator UnloadAsyncScene()
        {
            AsyncOperation asyncUnload = SceneManager.UnloadSceneAsync(_currentUnloadScene);

            while (!asyncUnload.isDone)
            {
                yield return null;
            }
            CompletedUnload?.Invoke();
        }
        
    }
}