﻿using System;
using Scripts.Player;
using UnityEngine;

namespace ScenesManager
{
    public class SceneController : MonoBehaviour
    {
        public event Action IsIniting;
        
        public PlayerModel PlayerModel { get; set; }
        public PlayerController PlayeController { get; set; }
        public InputController InputController { get; set; }

        public void Init(PlayerModel playerModel, InputController inputController, PlayerController playeController, Action del)
        {
            PlayerModel = playerModel;
            PlayeController = playeController;
            InputController = inputController;
            GetComponent<Game>().Finish += del;
            IsIniting.Invoke();
        }
    }
}