﻿using System;
using ScenesManager;
using Scripts.Interface;
using Scripts.Player;
using Scripts.Scriptable_Objects;
using UnityEngine;

public class Game : MonoBehaviour
{
    [SerializeField, Header("Настройки")] private LabyrinthData _labyrinthData;

    [SerializeField, Header("Родительские объекты")]
    private GameObject _wallsParent;

    [SerializeField] private GameObject _stakesParent;
    [SerializeField] private GameObject _doorParent;
    [SerializeField] private GameObject _floreParent;

    [SerializeField] private GameObject _playerViewPrefab;
    [SerializeField] private GameObject _playerViewParent;
    [SerializeField] private GameObject Interface;

    public event Action Finish;
    private SceneController _sceneController;
    private InterfaceController _interfaceController;
    private PlayerView _playerView;
    private Labyrinth.Labyrinth _labyrinth;

    private void Awake()
    {
        _sceneController = GetComponent<SceneController>();
        _sceneController.IsIniting += CustomStart;
    }

    private void CustomStart()
    {
        _sceneController.PlayeController.PlayerView = 
            Instantiate(_playerViewPrefab, _playerViewParent.transform).GetComponent<PlayerView>();
        _sceneController.PlayeController.Binding();

        _interfaceController = new InterfaceController(_sceneController.PlayerModel, Interface.GetComponent<InterfaceView>());
        _interfaceController.Binding();
        

        _labyrinth = new Labyrinth.Labyrinth(_labyrinthData);
        _labyrinth.Finish += NextLevel;
        _labyrinth.GenerateLabyrinth();

        _labyrinth.DrawLabyrinth(_wallsParent, _stakesParent, _doorParent, _floreParent);
    }

    private void NextLevel()
    {
        _sceneController.PlayerModel.CurrentLevel++;
        _sceneController.PlayeController.Unbinding();
        _interfaceController.Unbinding();
        Finish?.Invoke();
    }
}