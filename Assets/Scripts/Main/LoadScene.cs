﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadScene : MonoBehaviour
{
    [SerializeField] private string _levelName;
    [SerializeField] private Text loadingText;

    public string LevelName
    {
        get { return _levelName; }
        set { _levelName = value; }
    }

    void Start()
    {
        StartCoroutine("Fade");
        
//        SceneManager.LoadScene(1, LoadSceneMode.Additive);
//        gameObject.SetActive(false);
//        SceneManager.GetSceneAt(1)
            
    }

    private void LoadLevel(Scene s)
    {
        gameObject.SetActive(false);
    }


    private IEnumerator Fade()
    {
        for (int i = 0; i < 6; i++)
        {
            loadingText.text = "";
            for (int j = 0; j < i % 4; j++)
            {
                loadingText.text += ".";
            }
            yield return new WaitForSeconds(.3f);
        }
        SceneManager.LoadScene(1, LoadSceneMode.Additive);
        gameObject.SetActive(false);
        
    }
}

