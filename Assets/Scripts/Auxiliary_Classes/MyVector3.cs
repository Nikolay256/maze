﻿using System;
namespace Scripts.Auxiliary_Classes
{
    public class MyVector3
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }

        public MyVector3()
        {
        }
        
        public MyVector3(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public void LineClamp(float maxLen)
        {
            float len = (float)Math.Sqrt(X * X + Y * Y + Z * Z);
            if ( len <= maxLen) return;

            float k = maxLen / len;
            X *= k;
            Y *= k;
            Z *= k;
        }

        public static MyVector3 operator *(MyVector3 vector , float a)
        {
            return new MyVector3(vector.X * a, vector.Y * a, vector.Z * a); 
        }
    }
}