﻿using System;
using System.Collections.Generic;
using Scripts.Labyrinth.Walls;

namespace Scripts.Labyrinth.Generation_algorithms
{
    public class EllerAlgorithm
    {
        private Random _random;
        private int PRightWall;
        private int PDownWall;

        public EllerAlgorithm(int pRightWall, int pDownWall)
        {
            _random = new Random();
            PRightWall = pDownWall;
            PDownWall = pDownWall;
        }
        
        public void GenerateMaze(Walls.Walls maze)
        {
            for (int i = 0; i < maze.Width; i++)
            {
                maze[0, i].AddWall(Cell.UpWallFlag);
                maze[maze.Height - 1, i].AddWall(Cell.DownWallFlag);
            }

            for (int i = 0; i < maze.Height; i++)
            {
                maze[i, 0].AddWall(Cell.LeftWallFlag);
                maze[i, maze.Width - 1].AddWall(Cell.RightWallFlag);
            }

            ///TODO добавить возможность убирать стены от сюда
            //MazeArray[Height / 2, 0].RemoveWall(Cell.LeftWallFlag);
            //maze[maze.Height / 2, maze.Width - 1].RemoveWall(Cell.RightWallFlag);

            Dictionary<int, int> numObjectsInSets = new Dictionary<int, int>();
            int[] setsCurrentLine = new int[maze.Width];
            for (int i = 0; i < maze.Width; i++)
                setsCurrentLine[i] = i;

            for (int numLine = 0; numLine < maze.Height - 1; numLine++)
            {
                for (int i = 0; i < maze.Width - 1; i++)
                    if (_random.Next(0, 100) < PRightWall)
                        maze[numLine, i].AddWall(Cell.RightWallFlag);
                    else if (setsCurrentLine[i] == setsCurrentLine[i + 1])
                        maze[numLine, i].AddWall(Cell.RightWallFlag);
                    else
                    {
                        int temp = setsCurrentLine[i + 1];
                        for (int z = 0; z < setsCurrentLine.Length; z++)
                            if (setsCurrentLine[z] == temp)
                                setsCurrentLine[z] = setsCurrentLine[i];
                    }

                for (int i = 0; i < maze.Width; i++)
                    try
                    {
                        numObjectsInSets.Add(setsCurrentLine[i], 1);
                    }
                    catch (Exception)
                    {
                        numObjectsInSets[setsCurrentLine[i]]++;
                    }

                for (int i = 0; i < maze.Width; i++)
                    if (_random.Next(0, 100) < PDownWall && numObjectsInSets[setsCurrentLine[i]] != 1)
                    {
                        maze[numLine, i].AddWall(Cell.DownWallFlag);
                        numObjectsInSets[setsCurrentLine[i]]--;
                    }

                numObjectsInSets.Clear();

                CreateNewCurrentLine(setsCurrentLine, numLine, maze);
            }

            for (int i = 0; i < maze.Width - 1; i++)
            {
                if (maze[maze.Height - 2, i].HaveWall(Cell.RightWallFlag))
                    maze[maze.Height - 1, i].AddWall(Cell.RightWallFlag);
            }

            for (int i = 0; i < maze.Width - 1; i++)
                if (setsCurrentLine[i] != setsCurrentLine[i + 1])
                {
                    maze[maze.Height - 1, i].RemoveWall(Cell.RightWallFlag);

                    int temp = setsCurrentLine[i + 1];
                    for (int j = 0; j < setsCurrentLine.Length; j++)
                        if (setsCurrentLine[j] == temp)
                            setsCurrentLine[j] = setsCurrentLine[i];
                }
        }

        private void CreateNewCurrentLine(int[] currentLine, int numOldLine, Walls.Walls maze)
        {
            int max = currentLine[0];
            for (int i = 1; i < currentLine.Length; i++)
                if (currentLine[i] > max)
                    max = currentLine[i];

            int newSet = max + 1;
            for (int i = 0; i < currentLine.Length; i++)
                if (maze[numOldLine, i].HaveWall(Cell.DownWallFlag))
                    currentLine[i] = newSet++;
        }
    }
}