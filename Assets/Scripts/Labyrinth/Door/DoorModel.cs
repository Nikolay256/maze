﻿using System;

namespace Scripts.Labyrinth.Door
{
    public class DoorModel
    {
        public event Action IsDoorActiveChanged;
        
        public int NumLine { get; set; }
        public int NumColumn { get; set; }
        public byte Direction { get; set; }

        private bool _isDoorActive = false;
        public bool IsDoorActive
        {
            get => _isDoorActive;
            set
            {
                if(_isDoorActive != value)
                {
                    _isDoorActive = value;
                    IsDoorActiveChanged?.Invoke();
                }
            }
        }
        
        public DoorModel(int numLine, int numColumn, byte direction)
        {
            NumColumn = numColumn;
            NumLine = numLine;
            Direction = direction;
        }

        public void IsDoorActiveChanging()
        {
            IsDoorActive = true;
        }
    }
}