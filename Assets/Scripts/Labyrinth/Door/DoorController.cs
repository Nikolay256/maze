﻿namespace Scripts.Labyrinth.Door
{
    public class DoorController
    {
        public DoorModel _doorModel;
        public DoorView _doorView;
        public DoorController(DoorModel doorModel, DoorView doorView)
        {
            _doorView = doorView;
            _doorModel = doorModel;

            _doorView.OpenDoor += _doorModel.IsDoorActiveChanging;
        }
    }
}