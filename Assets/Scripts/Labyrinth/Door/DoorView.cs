﻿using System;
using UnityEngine;

namespace Scripts.Labyrinth.Door
{
    public class DoorView : MonoBehaviour
    {
        public event Action OpenDoor;
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.name == "Man(Clone)")
                OpenDoor?.Invoke();
        }
    }
}