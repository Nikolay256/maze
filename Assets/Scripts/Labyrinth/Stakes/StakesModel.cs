﻿using System;
using Scripts.Auxiliary_Classes;

namespace Scripts.Labyrinth.Stakes
{
    public class StakesModel
    {
        public int Damage { get; set; }
        public int NumLine { get; set; }
        public int NumColumn { get; set; }

        public StakesModel(int numLine, int numColumn, int damage = 3)
        {
            Damage = damage;
            NumColumn = numColumn;
            NumLine = numLine;
        }
    }
}