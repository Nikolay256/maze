﻿namespace Scripts.Labyrinth.Stakes
{
    public class StakesController
    {
        private StakesModel _stakesModel;
        private StakesView _stakesView;

        public StakesController(StakesModel stakesModel, StakesView stakesView)
        {
            _stakesModel = stakesModel;
            _stakesView = stakesView;
        }
    }
}