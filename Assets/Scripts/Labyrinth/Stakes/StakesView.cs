﻿using Scripts.Auxiliary_Classes;
using UnityEngine;

namespace Scripts.Labyrinth.Stakes
{
    public class StakesView : MonoBehaviour
    {
        public void Draw(MyVector3 position)
        {
            gameObject.transform.position = new Vector3(position.X, position.Y, position.Z);
        }
    }
}