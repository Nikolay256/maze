﻿using System;
using UnityEngine;
using Scripts.Scriptable_Objects;
using Scripts.Labyrinth.Door;
using Scripts.Labyrinth.Generation_algorithms;
using Scripts.Labyrinth.Stakes;
using Scripts.Labyrinth.Walls;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Labyrinth
{
    public class Labyrinth
    {
        public event Action Finish;
        
        private LabyrinthData _labyrinthData;
        private EllerAlgorithm _EllerAlgorithm;
        private StakesModel[] _stakesModels;
        private DoorModel _doorModel;
        private Walls _walls;
        private DrawWalls _drawWalls;

        public Labyrinth(LabyrinthData labyrinthData)
        {
            _labyrinthData = labyrinthData;
            
            _walls = new Walls(_labyrinthData.NumLines, _labyrinthData.NumColumns);
            
            _EllerAlgorithm = new EllerAlgorithm(50, 50);

            _stakesModels = new StakesModel[_labyrinthData.NumStakes];
        }

        public void GenerateLabyrinth()
        {
            _EllerAlgorithm.GenerateMaze(_walls);

            for (int i = 0; i < _labyrinthData.NumStakes; i++)
            {
                _stakesModels[i] = new StakesModel(Random.Range(0, _labyrinthData.NumLines),
                    Random.Range(0, _labyrinthData.NumColumns));
            }

            SetDoor(_labyrinthData.NumDoorLine, _labyrinthData.NumDoorColumn, _labyrinthData.DoorDirection);
        }

        public void DrawLabyrinth(GameObject wallsParent, GameObject stakesParent, GameObject doorParent, 
            GameObject floreParent)
        {
            if (_drawWalls == null) _drawWalls = new DrawWalls(_labyrinthData.WallPrefab, wallsParent);
            
            _drawWalls.Draw(_walls);
            DrawFlore(floreParent);
            DrawStakes(stakesParent);
            DrawDoor(doorParent);
        }

        private void SetDoor(int numLine, int numColumn, byte direction)
        {
            _walls[numLine, numColumn].RemoveWall(direction);
            _doorModel = new DoorModel(numLine, numColumn, direction);
        }
        
        private void DrawStakes(GameObject stakesParent)
        {
            StakesController tempStakesController;
            foreach (var model in _stakesModels)
            {
                GameObject temp = Object.Instantiate(_labyrinthData.StakesPrefab, stakesParent.transform);
                temp.transform.localPosition += new Vector3(model.NumColumn, 0f, model.NumLine);
                tempStakesController = new StakesController(model, temp.GetComponent<StakesView>());
            }
        }

        private void DrawDoor(GameObject doorParent)
        {
            int width = _labyrinthData.NumColumns;
            int i = _labyrinthData.NumDoorLine;
            int j = _labyrinthData.NumDoorColumn;
            
            GameObject temp = Object.Instantiate(_labyrinthData.DoorPrefab, doorParent.transform);

            switch (_labyrinthData.DoorDirection)
            {
                case Cell.DownWallFlag:
                    temp.transform.localPosition += new Vector3(width - j - 1, 0, i + 0.5f);
                    break;
                case Cell.RightWallFlag:
                    temp.transform.localRotation = Quaternion.AngleAxis(90, Vector3.up);
                    temp.transform.localPosition += new Vector3(width - (j + 1.5f), 0, i);
                    break;
                case Cell.LeftWallFlag:
                    temp.transform.localRotation = Quaternion.AngleAxis(90, Vector3.up);
                    temp.transform.localPosition += new Vector3(width - (j + 0.5f), 0, i);
                    break;
                case Cell.UpWallFlag:
                    temp.transform.localPosition += new Vector3(width - j - 1, 0, i - 0.5f);
                    break;
            }

            var temDoorController = new DoorController(_doorModel, temp.GetComponent<DoorView>());
            temDoorController._doorView.OpenDoor += Finish;
        }

        private void DrawFlore(GameObject floreParent)
        {
            for (int i = 0; i < _labyrinthData.NumLines; i++)
            {
                for (int j = 0; j < _labyrinthData.NumColumns; j++)
                {
                    GameObject temp = Object.Instantiate(_labyrinthData.FlorePrefab, floreParent.transform);
                    temp.transform.localPosition += new Vector3(j, 0f, i);
                }
            }
        }
    }
}