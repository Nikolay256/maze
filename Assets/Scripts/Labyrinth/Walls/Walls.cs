﻿namespace Scripts.Labyrinth.Walls
{
    public class Walls
    {
        
        public int Height { get; }
        public int Width { get; }
        private Cell[,] _mazeArray;

        public Walls(int height, int width)
        {
            Height = height;
            Width = width;
            _mazeArray = new Cell[height, width];
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    _mazeArray[i, j] = new Cell(0);
                }
            }
        }
        
        public Cell this[int index1, int index2]
        {
            get
            { return _mazeArray[index1, index2]; }
            set
            { _mazeArray[index1, index2] = value; }
        }
        
    }
}