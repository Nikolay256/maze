﻿using UnityEngine;

namespace Scripts.Labyrinth.Walls
{
    public class DrawWalls
    {
        private GameObject WallPrefab;
        private GameObject WallsParent;

        public DrawWalls(GameObject wallPrefab, GameObject wallsParent)
        {
            WallPrefab = wallPrefab;
            WallsParent = wallsParent;
        }

        public void Draw(Walls maze)
        {
            for (int i = 0; i < maze.Height; i++)
            for (int j = 0; j < maze.Width; j++)
            {
                if (maze[i, j].HaveWall(Cell.DownWallFlag))
                {
                    GameObject temp = Object.Instantiate(WallPrefab, WallsParent.transform);
                    temp.transform.localPosition += new Vector3(maze.Width - j - 1, 0, i + 0.5f);
                }

                if (maze[i, j].HaveWall(Cell.RightWallFlag))
                {
                    GameObject temp = Object.Instantiate(WallPrefab, WallsParent.transform);
                    temp.transform.localRotation = Quaternion.AngleAxis(90, Vector3.up);
                    temp.transform.localPosition += new Vector3(maze.Width - (j + 1.5f), 0, i);
                }

                if (maze[i, j].HaveWall(Cell.LeftWallFlag))
                {
                    GameObject temp = Object.Instantiate(WallPrefab, WallsParent.transform);
                    temp.transform.localRotation = Quaternion.AngleAxis(90, Vector3.up);
                    temp.transform.localPosition += new Vector3(maze.Width - (j + 0.5f), 0, i);
                }

                if (maze[i, j].HaveWall(Cell.UpWallFlag))
                {
                    GameObject temp = Object.Instantiate(WallPrefab, WallsParent.transform);
                    temp.transform.localPosition += new Vector3(maze.Width - j - 1, 0, i - 0.5f);
                }
            }
        }
    }
}