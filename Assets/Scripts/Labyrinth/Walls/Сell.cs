﻿using System;

namespace Scripts.Labyrinth.Walls
{
    public class Cell
    {
        public const byte UpWallFlag    = 0b1000;
        public const byte LeftWallFlag  = 0b0100;
        public const byte RightWallFlag = 0b0010;
        public const byte DownWallFlag  = 0b0001;

        private byte _cell;

        public Cell(byte wallInfo)
        {
            if (wallInfo >= 0b1111)
                throw new Exception("Превышено максимальное значение 'wallInfo'. Ожидалось не более 0b1111");
            _cell = wallInfo;
        }

        public void AddWall(byte wallInfo)
        {
            if (wallInfo >= 0b1111)
                throw new Exception("Превышено максимальное значение 'wallInfo'. Ожидалось не более 0b1111");
            _cell |= wallInfo;
        }

        public void RemoveWall(byte wallInfo)
        {
            if (wallInfo >= 0b1111)
                throw new Exception("Превышено максимальное значение 'wallInfo'. Ожидалось не более 0b1111");
            _cell &= (byte)~wallInfo;
        }

        public bool HaveWall(byte wallInfo)
        {
            if (wallInfo >= 0b1111)
                throw new Exception("Превышено максимальное значение 'wallInfo'. Ожидалось не более 0b1111");
            return (_cell & wallInfo) == wallInfo;
        }
    }
}
